use csv;
use std;
use std::net::IpAddr;

use ip_network::IpNetwork;

use std::str::FromStr;

use std::net::{Ipv4Addr, Ipv6Addr};
use treebitmap::IpLookupTable;

#[derive(Deserialize, Debug)]
struct Row {
    network: String,
    autonomous_system_number: u64,
    autonomous_system_organization: String,
}

pub struct GeoIp {
    v4mapping: IpLookupTable<Ipv4Addr, Row>,
    v6mapping: IpLookupTable<Ipv6Addr, Row>,
}

impl GeoIp {
    pub fn new(csv_path: &str) -> Result<GeoIp, std::io::Error> {
        //let csv_path = "src/data/GeoLite2-ASN-CSV_20180709/GeoLite2-ASN-Blocks-IPv4.csv";
        let mut rdr = csv::ReaderBuilder::new()
            .delimiter(b',')
            .from_path(csv_path)?;
        let mut v4mapping = IpLookupTable::new();
        let mut v6mapping = IpLookupTable::new();
        let records = rdr.deserialize().collect::<Result<Vec<Row>, _>>()?;
        records
            .into_iter()
            .for_each(|row| match parse_addr(&row.network) {
                None => eprintln!("Failed to parse network: {}", &row.network),
                Some(IpNetwork::V4(v4)) => {
                    v4mapping.insert(v4.network_address(), v4.netmask().into(), row);
                }
                Some(IpNetwork::V6(v6)) => {
                    v6mapping.insert(v6.network_address(), v6.netmask().into(), row);
                }
            });
        Ok(GeoIp {
            v4mapping,
            v6mapping,
        })
    }

    pub fn get_as(&self, addr: IpAddr) -> Option<u64> {
        match addr {
            IpAddr::V4(v4) => self
                .v4mapping
                .longest_match(v4)
                .map(|e| e.2.autonomous_system_number),
            IpAddr::V6(v6) => self
                .v6mapping
                .longest_match(v6)
                .map(|e| e.2.autonomous_system_number),
        }
    }
}

fn parse_addr(string: &str) -> Option<IpNetwork> {
    IpNetwork::from_str(string).ok()
}

#[test]
fn test_geoip() {
    let geoip = GeoIp::new("src/data/GeoLite2-ASN-CSV_20180709/geolite.csv")
        .expect("can't parse geolite.csv");
    assert_eq!(
        geoip.get_as(IpAddr::from_str("137.226.107.63").unwrap()),
        Some(47610)
    );
    assert_eq!(
        geoip.get_as(IpAddr::from_str("2a00:fe0:0:3::1000").unwrap()),
        Some(34953)
    );
    assert_eq!(geoip.get_as(IpAddr::from_str("ffff::").unwrap()), None);
}
