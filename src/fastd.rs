use std::collections::HashMap;

#[derive(Deserialize, Debug)]
#[allow(unused)]
pub struct FastdInfo {
    pub peers: Option<HashMap<String, FastdPeer>>,
}

#[derive(Deserialize, Debug)]
#[allow(unused)]
pub struct FastdPeer {
    pub connection: Option<FastdConnection>,
    pub address: String,
    pub name: Option<String>,
}

#[derive(Deserialize, Debug)]
#[allow(unused)]
pub struct FastdConnection {
    pub mac_addresses: Vec<String>,
    pub statistics: FastdStatistics,
    pub method: String,
    pub established: u64,
}

#[derive(Deserialize, Debug)]
#[allow(unused)]
pub struct FastdStatistics {
    pub tx_error: FastdCounter,
    pub tx_dropped: FastdCounter,
    pub tx: FastdCounter,
    pub rx_reordered: FastdCounter,
    pub rx: FastdCounter,
}

#[derive(Deserialize, Debug)]
#[allow(unused)]
pub struct FastdCounter {
    pub bytes: u64,
    pub packets: u64,
}
