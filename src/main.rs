
use serde_json;
#[macro_use]
extern crate serde_derive;

#[macro_use]
extern crate log;
use syslog;
use log_panics;

mod accumulator;
mod actix;
mod fastd;
mod geoip;

use crate::fastd::FastdInfo;

use std::ffi::OsStr;
use std::io::prelude::*;
use std::iter::IntoIterator;
use std::os::unix::net::UnixStream;

use std::str::FromStr;
use std::thread;

use crate::geoip::GeoIp;
use std::net::SocketAddr;

use std::time;

use clap::App;

use crate::accumulator::Accumulator;
use crate::accumulator::AccumulatorError;

use std::collections::HashMap;

struct FastdData {
    key: String,
    addr: String,
    bytes: u64,
}

fn add_fastd_sums(accumulator: &mut Accumulator, sums: &[(FastdData, u64)]) {
    sums.iter()
        .for_each(|(data, as_number)| accumulator.add_entry(&data.key, *as_number, data.bytes));
}

fn connection_counts(sums: &[(FastdData, u64)]) -> HashMap<u64, u64> {
    let mut counts: HashMap<u64, u64> = HashMap::new();

    sums.iter()
        .for_each(|(_, as_number)| {
            let entry = counts.entry(*as_number).or_insert(0);
            *entry = entry.saturating_add(1);
        });
    counts
}


fn read_traffic_from_fastds<'a, T>(sockets: T) -> Vec<FastdData>
where
    T: IntoIterator<Item = &'a OsStr>,
{
    sockets
        .into_iter()
        .flat_map(|socket| read_traffic_from_fastd(socket).into_iter())
        .collect()
}

fn main() {
    init_log().expect("Failed to initialize logging");

    let matches = App::new("ff-provider-statistics")
        .version("0.0.0")
        .author("@jannic")
        .args_from_usage(
            "
            --print-ips     Print IP->AS mapping and exit
            <geoip>         CSV mapping ip networks to AS numbers
            <socket>...     Fastd sockets to read connection info from",
        ).get_matches();

    let geoip = GeoIp::new(matches.value_of("geoip").expect("geoip argument missing"))
        .expect("failed to read geoip file");

    let mut acc = Accumulator::new();
    // XXX generalize to many sockets
    let sockets = matches
        .values_of_os("socket")
        .expect("socket argument missing");

    let counter = map_ips_to_as(&geoip, read_traffic_from_fastds(sockets.clone()));
    if matches.is_present("print-ips") {
        counter
            .iter()
            .for_each(|(data, as_number)| println!("{} {}", data.addr, as_number));
        return;
    }

    add_fastd_sums(
        &mut acc,
        &counter,
    );
    reset_or_panic(&mut acc);

    let state = actix::AppState::new();
    let thread_state = state.clone();
    thread::spawn(|| actix::start_actix(thread_state));
    let mut reported = std::collections::HashSet::new();
    loop {
        std::thread::sleep(time::Duration::from_millis(1000));
        let counter = map_ips_to_as(&geoip, read_traffic_from_fastds(sockets.clone()));
        add_fastd_sums(&mut acc, &counter);
        let counts = connection_counts(&counter);
        report_as201701(&counter, &mut reported);
        // XXX remove lost connections
        let mut lock = state.locked();
        match acc.get_sums() {
            Ok(sums) => *lock = (sums, counts),
            Err(AccumulatorError::Overflow) => reset_or_panic(&mut acc),
        }

        //println!("Results: {:?}", *lock);
    }

    //let mapped = map_ips_to_as(&geoip, sums);
    //println!("Results:");
    //mapped.iter().for_each( |p| println!("{:?}", p ) );
}

fn reset_or_panic(acc: &mut Accumulator) {
    if let Err(e) = acc.reset_sums() {
        error!("Failed to reset counters: {:?}", e);
        panic!("Failed to reset counters: {:?}", e);
    }
}

fn map_ips_to_as(geoip: &GeoIp, sums: Vec<FastdData>) -> Vec<(FastdData, u64)> {
    sums.into_iter()
        .flat_map(|data| {
            if let Ok(a) = SocketAddr::from_str(&data.addr) {
                let opt_as = geoip.get_as(a.ip());
                /*
                if opt_as.is_none() {
                    println!("No AS found for {}",a.ip());
                }
                */
                Some((data, opt_as.unwrap_or(0)))
            } else {
                println!("Can't parse {}", data.addr);
                None
            }
        }).collect()
}

fn report_as201701(data: &Vec<(FastdData, u64)>, reported: &mut std::collections::HashSet<String>) {
    data.iter()
        .filter(|(data, as_number)| *as_number == 201701 && data.bytes != 0)
        .for_each(|(data, _)| {
            if !reported.contains(&data.addr) {
                info!(
                    "Connection from AS 201701. IP: {}, Key: {}",
                    data.addr, data.key
                );
                reported.insert(data.addr.clone());
            }
        });
}

fn read_traffic_from_fastd(path: &OsStr) -> Vec<FastdData> {
    let mut stream = match UnixStream::connect(path) {
        Ok(stream) => stream,
        Err(e) => {
            error!("unable to open stream at {:?}: {}", path, e);
            return Vec::new();
        },
    };
    let mut response = String::new();
    if let Err(e) = stream.read_to_string(&mut response) {
        error!("Failed to read fastd socket at {:?}: {}", path, e);
        return Vec::new();
    }
    let i: FastdInfo = match serde_json::from_str(&response) {
        Ok(i) => i,
        Err(e) => {
            error!("Failed parsing contents of fastd socket at {:?}: {}", path, e);
            return Vec::new();
        },
    };

    if let Some(peers) = i.peers {
        let sums: Vec<_> = peers
            .iter()
            .filter(|(_, v)| v.connection.is_some())
            .map(|(k, v)| FastdData {
                key: k.clone(),
                addr: v.address.clone(),
                bytes: sum_counters(v.connection.as_ref().unwrap()),
            }).collect();

        sums
    } else {
        Vec::new()
    }
}

fn sum_counters(conn: &fastd::FastdConnection) -> u64 {
    let stat = &conn.statistics;
    stat.tx.bytes + stat.rx.bytes
}

fn init_log() -> syslog::Result<()> {
    let facility = syslog::Facility::LOG_USER;
    let log_level = log::LevelFilter::Info;
    syslog::init_unix(facility, log_level)?;
    log_panics::init();
    Ok(())
}
