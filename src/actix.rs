#![cfg_attr(feature = "cargo-clippy", allow(needless_pass_by_value))]

use actix_web::http::header::IntoHeaderValue;
use actix_web::http::header::{CacheControl, CacheDirective};
use actix_web::middleware::{Middleware, Response};
use actix_web::HttpResponse;
use actix_web::{http::header, http::Method, server::HttpServer, App, HttpRequest, Json, Result};

use std::collections::HashMap;
use std::sync::Arc;
use std::sync::{Mutex, MutexGuard};

#[derive(Clone)]
pub struct AppState {
    data: Arc<Mutex<(HashMap<u64, u64>, HashMap<u64, u64>)>>,
}

impl AppState {
    pub fn new() -> AppState {
        AppState {
            data: Arc::new(Mutex::new((HashMap::new(), HashMap::new()))),
        }
    }
    fn get_sums(&self) -> Result<HashMap<u64, u64>> {
        Ok(self.locked().0.clone())
    }
    fn get_counts(&self) -> Result<HashMap<u64, u64>> {
        Ok(self.locked().1.clone())
    }
    pub fn locked(&self) -> MutexGuard<'_, (HashMap<u64, u64>, HashMap<u64, u64>)> {
        self.data.lock().unwrap()
    }
}

fn index(req: &HttpRequest<AppState>) -> Result<Json<HashMap<u64, u64>>> {
    let data = req.state().get_sums()?;
    Ok(Json(data))
}

fn counts(req: &HttpRequest<AppState>) -> Result<Json<HashMap<u64, u64>>> {
    let data = req.state().get_counts()?;
    Ok(Json(data))
}

pub fn start_actix(state: AppState) {
    HttpServer::new(move || {
        App::with_state(state.clone())
            .middleware(NoCacheHeaders)
            .resource("/", |r| r.method(Method::GET).f(index))
            .resource("/counts", |r| r.method(Method::GET).f(counts))
    }).bind("[::]:8232")
    .unwrap()
    .disable_signals()
    .run();
}

struct NoCacheHeaders;

impl<S> Middleware<S> for NoCacheHeaders {
    fn response(&self, _req: &HttpRequest<S>, mut resp: HttpResponse) -> Result<Response> {
        resp.headers_mut().insert(
            header::CACHE_CONTROL,
            CacheControl(vec![CacheDirective::NoCache, CacheDirective::MaxAge(1u32)]).try_into()?,
        );
        Ok(Response::Done(resp))
    }
}
