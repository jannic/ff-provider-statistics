use std::collections::HashMap;

pub struct Accumulator {
    per_node: HashMap<String, NodeInfo>,
    base_values: HashMap<u64, BaseValueType>,
}

type BaseValueType = i128;

#[derive(Debug)]
pub enum AccumulatorError {
    Overflow,
}

struct NodeInfo {
    as_number: u64,
    bytes: u64,
}

impl Accumulator {
    pub fn new() -> Accumulator {
        Accumulator {
            per_node: HashMap::new(),
            base_values: HashMap::new(),
        }
    }

    fn get_raw_sums(&self) -> Result<HashMap<u64, u64>, AccumulatorError> {
        let mut sums: HashMap<u64, u64> = HashMap::new();
        let mut has_overflow = false;
        self.per_node.values().for_each(|v| {
            let entry = sums.entry(v.as_number).or_insert(0);
            if let Some(sum) = entry.checked_add(v.bytes) {
                *entry = sum;
            } else {
                has_overflow = true;
            }
        });
        if has_overflow {
            Err(AccumulatorError::Overflow)
        } else {
            Ok(sums)
        }
    }

    pub fn get_sums(&self) -> Result<HashMap<u64, u64>, AccumulatorError> {
        let mut sums = self.get_raw_sums()?;
        let mut has_overflow = false;
        sums.iter_mut().for_each(|(k, v)| {
            let new_v: i128 = i128::from(*v) - self.base_values.get(k).unwrap_or(&0);
            if new_v >= 0 && new_v <= u64::max_value().into() {
                *v = new_v as u64;
            } else {
                has_overflow = true;
            }
        });
        if has_overflow {
            Err(AccumulatorError::Overflow)
        } else {
            Ok(sums)
        }
    }

    pub fn reset_sums(&mut self) -> Result<(), AccumulatorError> {
        self.base_values = self
            .get_raw_sums()?
            .iter()
            .map(|(k, v)| (*k, i128::from(*v)))
            .collect();
        info!(
            "Resetting traffic counters. New base values are: {:?}",
            self.base_values
        );
        Ok(())
    }

    pub fn add_entry(&mut self, node: &str, as_number: u64, counter: u64) {
        {
            // split borrows outside of closure
            let per_node = &mut self.per_node;
            let base_values = &mut self.base_values;
            per_node
                .entry(node.to_string())
                .and_modify(|v| {
                    if v.as_number != as_number {
                        Self::move_as_bytes(base_values, v.as_number, as_number, v.bytes);
                    }
                    if v.bytes > counter {
                        // counter decreased - assume it restarted from zero
                        let base = base_values.entry(as_number).or_insert(0);
                        *base -= BaseValueType::from(v.bytes);
                    }
                    v.as_number = as_number;
                    v.bytes = counter;
                }).or_insert(NodeInfo {
                    as_number,
                    bytes: counter,
                });
        }
    }

    /// Move aggregate values from one AS to another one
    ///
    /// Calling this is necessary if an open connection moves to a different
    /// AS, which is possibe with fastd.
    fn move_as_bytes(
        base_values: &mut HashMap<u64, BaseValueType>,
        from: u64,
        to: u64,
        bytes: u64,
    ) {
        {
            let v_from = base_values.entry(from).or_insert(0);
            *v_from -= BaseValueType::from(bytes)
        }
        {
            let v_to = base_values.entry(to).or_insert(0);
            *v_to += BaseValueType::from(bytes);
        }
    }

    #[allow(dead_code)] // unused, for now
    fn delete_entry(&mut self, node: &str) {
        if let Some((_, before)) = self.per_node.remove_entry(node) {
            self.base_values
                .entry(before.as_number)
                .and_modify(|v| *v -= BaseValueType::from(before.bytes));
        }
    }
}

#[test]
fn test_simple_counting() {
    let mut acc = Accumulator::new();
    acc.add_entry("n1", 1, 1000);
    acc.add_entry("n2", 2, 2000);
    let sums = acc.get_sums().unwrap();
    assert_eq!(sums.len(), 2);
    assert_eq!(sums[&1], 1000);
    assert_eq!(sums[&2], 2000);
}

#[test]
fn test_incremental_counting() {
    let mut acc = Accumulator::new();
    acc.add_entry("n1", 1, 1000);
    acc.add_entry("n2", 2, 2000);
    acc.add_entry("n1", 1, 3000);
    acc.add_entry("n1", 1, 4000);
    acc.add_entry("n2", 2, 2000);
    acc.add_entry("n1b", 1, 1000);
    let sums = acc.get_sums().unwrap();
    assert_eq!(sums.len(), 2);
    assert_eq!(sums[&1], 5000);
    assert_eq!(sums[&2], 2000);
}

#[test]
fn test_reset_base() {
    let mut acc = Accumulator::new();
    acc.add_entry("n1", 1, 1000);
    acc.add_entry("n2", 2, 2000);
    acc.add_entry("n1", 1, 3000);
    acc.reset_sums().unwrap();
    acc.add_entry("n1", 1, 4000);
    acc.add_entry("n2", 2, 2000);
    acc.add_entry("n1b", 1, 1000);
    let sums = acc.get_sums().unwrap();
    assert_eq!(sums.len(), 2);
    assert_eq!(sums[&1], 2000);
    assert_eq!(sums[&2], 0);
}

#[test]
fn test_switch_as() {
    let mut acc = Accumulator::new();
    acc.add_entry("n1", 1, 1000);
    acc.add_entry("n2", 2, 2000);
    acc.add_entry("n1b", 1, 5000);
    acc.reset_sums().unwrap(); // 8000
    assert_eq!(acc.base_values[&1], 6000);
    acc.add_entry("n1", 1, 3000);
    assert_eq!(acc.base_values[&1], 6000);
    acc.add_entry("n1", 3, 4000);
    assert_eq!(acc.base_values[&1], 3000);
    acc.add_entry("n2", 2, 2000);
    acc.add_entry("n1b", 1, 10000);
    let sums = acc.get_sums().unwrap(); // 16000 - 8000 = 8000
    assert_eq!(sums.len(), 3);
    assert_eq!(sums[&1], 7000);
    assert_eq!(sums[&2], 0);
    assert_eq!(sums[&3], 1000);
}

#[test]
fn test_switch_as_no_base() {
    let mut acc = Accumulator::new();
    acc.add_entry("n1", 1, 1000);
    acc.add_entry("n2", 2, 2000);
    acc.add_entry("n1b", 1, 5000);
    acc.add_entry("n1", 1, 3000);
    acc.add_entry("n1", 3, 4000);
    acc.add_entry("n2", 2, 2000);
    let sums = acc.get_sums().unwrap();
    assert_eq!(sums.len(), 3);
    assert_eq!(sums[&1], 8000);
    assert_eq!(sums[&2], 2000);
    assert_eq!(sums[&3], 1000);
}

#[test]
fn test_counter_decreased_new() {
    let mut acc = Accumulator::new();
    acc.add_entry("n0", 1, 10000); // to have sufficiently high base value
    acc.reset_sums().unwrap();
    acc.add_entry("n1", 1, 1000);
    acc.add_entry("n1", 1, 2000);
    acc.add_entry("n1", 1, 3000);
    acc.add_entry("n1", 1, 1500); // should not reset but add
    acc.add_entry("n1", 1, 2500);
    let sums = acc.get_sums().unwrap();
    assert_eq!(sums.len(), 1);
    assert_eq!(sums[&1], 5500);
}

#[test]
fn test_counter_decreased_and_switched_as() {
    let mut acc = Accumulator::new();
    acc.add_entry("n0", 1, 10000); // to have sufficiently high base value
    acc.reset_sums().unwrap();
    acc.add_entry("n1", 1, 1000);
    acc.add_entry("n1", 1, 2000);
    acc.add_entry("n1", 1, 3000);
    acc.add_entry("n1", 2, 1500); // should not reset but add
    acc.add_entry("n1", 2, 2500);
    let sums = acc.get_sums().unwrap();
    assert_eq!(sums.len(), 2);
    assert_eq!(sums[&1], 3000);
    assert_eq!(sums[&2], 2500);
}
