# ff-provider-statistics

This is a Freifunk-Aachen specific tool to aggregate traffic statistics by
internet access provider.

Several details of our infrastructure are hard coded, so this software can't
easily be used by others. But as it's open source, you are free to adapt it to
your needs or extract functions which are useful to you.


## License

The software in this reporitory is distributed under the terms of both the MIT license
and the Apache License (Version 2.0).

Dependencies use different licenses, so if you use or distribute binaries, please
read and follow those, as well.

See [LICENSE-APACHE](LICENSE-APACHE), [LICENSE-MIT](LICENSE-MIT) for details.

Data files in [src/data/GeoLite2-ASN-CSV_20180709/](src/data/GeoLite2-ASN-CSV_20180709/) have been downloaded from
https://dev.maxmind.com/geoip/geoip2/geolite2/ and are distributed under
the [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
See [COPYRIGHT](src/data/GeoLite2-ASN-CSV_20180709/COPYRIGHT.txt) and [LICENSE](src/data/GeoLite2-ASN-CSV_20180709/LICENSE.txt)
for details about the license of GeoLite2.
